﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SUMA
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Inici : ContentPage
    {
        public Inici()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public Inici(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
    }

}