﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SUMA
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FiJoc : ContentPage
    {
        public FiJoc()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }

        public FiJoc(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        //public FiJoc(int a)
        //{
        //    InitializeComponent();
        //    BindingContext = new MainPageViewModel(this, a);
        //}

    }
}